
class Country{
    constructor(name) {
        this.name = name
        this.cities = []

    }
    addCity(loc) {

        for (let i = 0; i < loc.length; i++) {
            //this.cities.splice(i,0,loc[i])
            this.cities.push(loc[i])
        }
    }
    delCity(cityName){
        let index = this.cities.indexOf(cityName);
        this.cities.splice(index, 1);
    }
}

module.exports = {Country}